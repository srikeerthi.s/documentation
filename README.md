# Documentation

Template with guidelines for creating documentation of any project

## Getting started

- Create directory `/docs` in the project
- Copy the files [guidelines.md](/guidelines.md) and [research.md](/research.md) into `/docs` directory
- Edit as per the requirements