# Title of project
Provide description about the project and the problem that is solved in 2-3 lines

## Table of Contents
#### Sample:
- [General flow of project](#general-flow-of-project)
- [Installation](#installation)
- [Directory structure](#directory-structure)
- [Information about directory/files](#information-about-directoryfiles)
- [Steps to run](#steps-to-run)
- [Results](#results)
- [Definitions](#definitions)

## General flow of project
A flowchart will help any developer understand the overall flow of the program

- Generate diagrams and flowcharts from text by using [Mermaid](https://mermaidjs.github.io/) or [PlantUML](https://plantuml.com/).

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
    OR 

- Generate a flowchart using [draw.io](https://draw.io) 

![alt text](/images/sample.png "architecture/data flow")

## Installation 
- Clone specific branch instead of entire repository
    ```
    git clone --depth 1 <url> -b branch_name
    ```
- Create virtual environment using conda or python venv
    ```
    python3 -m venv env_name
    source env_name/bin/activate
    ```
- Command to install requirements 
    ```
    pip install -r requirements.txt
    ```
## Directory structure
Directory structure will give clear idea on directory/files that exist once cloning is performed


#### Sample:
```
|── recipe_scrapping
    |── crawled (exists after scrapping a website)
    │── meal_scrapping
	    │── spiders
            |── allrecipe_spider.py
            |── fatsecret_spider.py
            |── iiitd_spider.py
            |── indian_healthy_spider.py
            |── taradalal_spider.py
            |── vegrecipe_spider.py
    │── items.py
    |── middlewares.py
    |── pipelines.py
    |── settings.py
    |── utils
        |── mongo_utils.py
|── scrapping (environment dir)     
```

## Information about directory/files
Create a table listing the important directories and files with a description 

#### Sample:

| Directory/File Name | Description|
--- |---
crawled/ | Consists sub directories of each spider with history of encrypted links visited during scrapping
meal_scrapping/ | Consists of files and subdirectories required to run the spider
spiders/ | Main directory with custom spider to crawl each website
items.py | Script that contains variables that store scrapped data
middlewares.py | Script that handles spiders middleware
pipelines.py | Script that processes scrapped data
settings.py | Settings to manage the spider
mongo_utils.py | Script that connects to the mongodb database with functions of CRUD operations

## Steps to run
- Provide the steps and commands to run the program. 
- If there are different arguments in the command provide a table with arguments

#### Sample:
- execute from directory meal_scrapping/
    ```
    cd meal_scrapping/
    scrapy crawl <spider_name>
    ```
- Various spiders are as shown in table
    | spider_name | Website |
    --- | ---
    vegrecipe | [Vegrecipe](https://www.vegrecipesofindia.com)
    taradalal | [Taradalal](https://www.tarladalal.com)
    iiitd | [iiitd recipedb](https://cosylab.iiitd.edu.in/recipedb/)
    fatsecret | [fatsecret](https://www.fatsecret.co.in)
    indianhealthyrecipe | [Indian healthy recipe](https://www.indianhealthyrecipes.com)
    allrecipe | [allrecipe](allrecipe.com )

## Results
- Provide image or sample result
- If the data is stored in different databases, provide details about collection/db_name 

#### Sample:
Data is stored in mongodb under the following collections:

| Spider name | collection name |
--- | ---
 vegrecipe | vegrecipe
 taradalal | taradalal
 iiitd | iiitd 
 fatsecret | fatsecret
 indianhealthyrecipe | indianhealthy
 allrecipe | allrecipe

## Definitions
Brief explaination about the technical terms used throughout the readme will be helpful for developers as it saves time from performing research on what the terms mean

#### Sample:

- spiders : Classes which define the custom behaviour for crawling and parsing pages a particular site (or, in some cases, a group of sites).

- crawl : process of going through links and data

- scrape : process of extracting data from web pages

- items : Spiders return extracted data usually in form of dictionaries

- pipeline :  Python class that implements a simple method. They receive an item and perform an action over it. Actions can include saving data to databases, send data to client, etc.