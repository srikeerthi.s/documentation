# Reseach work 
This is a documentation of the various libraries, tools that were used to create a prototype.

This record helps in identifying various features, compares various frameworks used before the final implementation

## Task name
- Library/Framework name
- time spent
- features/findings
- links of articles/journals reffered
- Any other details that benefits

### Sample

#### Research of scrapping library
- Framework: Scrapy
- About Framework: Helps in scrapping data from website,csv,xml
- Time spent: 4 weeks
- Features:
    - Compatible with xpath commands and css
    - Integration of beautiful soup library also possible
- Links: [Scrapy](https://docs.scrapy.org/)